#!/usr/bin/env python
# Copyright (c) 2021, Pensando Systems
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
# Author(s): Edward Arcuri edward@pensando.io
#
from hashlib import new
import time
import random
import socket
from lib import taor
from datetime import datetime, timedelta, timezone



def buildTLog(timestamp,hostName,devName,swVer,sn,na_pid,eventList):

  # Set init variables
  ipkt = 0
  ibytes = 0
  rpkt = 0
  rbytes = 0
  sessionID = random.randint(5000,419999)
  dsm = random.randint(1,2)
  initCSV = ""
  endCSV = ""
  initMESG = ""
  endMESG = ""
  initHeader = (f"<14>1 {timestamp} {hostName} pen-netagent {na_pid} - - ")
  endHeader = ""

  for entry in eventList:

    initCSV = f"{timestamp},flow_create,{entry['action']},{entry['vrf']},{entry['srcIP']},{entry['srcPort']},{entry['dstIP']},{entry['dstPort']},{entry['protocol']},{sessionID},{entry['policyID']},{entry['ruleHash']},{entry['ruleName']},{ipkt},{ibytes},{rpkt},{rbytes},{entry['srcVLAN']},DSS,{swVer},{sn},{devName},{dsm},v3,{entry['policyName']},{entry['policyName']}_CM\n"
              # 2022-01-31T16:41:34Z,flow_create,allow,   94aa3974-e515-4f74-af3f-b81a4228c079,10.6.201.106,42666,185.125.190.27,443,             6,             12583469,83912d45-c6b4-4d5e-bc95-ebccd653618e,2514173733807309214,blockICMP-Internet-Services-HTTPS,0,0,0,0, 1201,             DSS, DL.10.09.0003B,FSJ21350020,0490.8100.5800,2,v3
              # 2021-01-25T11:02:26Z,flow_create,allow,   1029,                                10.1.1.166,27272,  8.8.8.8,        53,             17,              362955,1bc8fe12-c4a3-78bc-ca8e-ac98712fcf9b,973940428850155584288741,allow-dns,0,0,0,0,                    110,              DSS, DL.XX.XX.XXXX, FSJ212800090490.8100.1bee,1,v3
    initMESG = initMESG + initCSV

    # If we are generating an allow, we must also generate a session end (flow_delete) log with packet and byte counts.
    if entry['action'] == "allow":
      random.seed(sessionID)
      ipkt = random.randint(82,10010092)
      ibytes = ipkt * 1500
      random.seed(ipkt)
      percentage = round(random.randint(0, 2) * random.random(), 2)
      rpkt = int(round(ipkt * percentage))
      rbytes = rpkt * 1499

    # Create a seed based on the number of packets sent.  Add 1 to it because randrange explodes if we
    # ever send it 0.
    offsetSeed = int(round(ipkt/15000)+1)
    timeOffset = random.randint(1,offsetSeed)

    # Generate the "future" timestamp for when the session ends.
    endTimestamp = f"{datetime.strptime(timestamp, '%Y-%m-%dT%H:%M:%SZ') + timedelta(seconds=timeOffset):%Y-%m-%dT%H:%M:%SZ}"
    # FIXME: This gets done each time - it's late and I am not clear on how to fix this yet - but it works. It's also
    # immaterial to how our logs get ingested, so it may not be a big deal to fix.
    endHeader = (f"<14>1 {endTimestamp} {hostName} pen-netagent {na_pid} - - ")
    # Create the session end log with new timestamp and pkt and byte counts filled in.
    endCSV = f"{endTimestamp},flow_delete,{entry['action']},{entry['vrf']},{entry['srcIP']},{entry['srcPort']},{entry['dstIP']},{entry['dstPort']},{entry['protocol']},{sessionID},{entry['policyID']},{entry['ruleHash']},{entry['ruleName']},{ipkt},{ibytes},{rpkt},{rbytes},{entry['srcVLAN']},DSS,{swVer},{sn},{devName},{dsm},v3,{entry['policyName']},{entry['policyName']}_CM\n"
    endMESG = endMESG + endCSV


  initLog = initHeader + initMESG
  endLog = endHeader + endMESG

  return initLog,endLog


def genRandomIP(network):
  """
  Generate a random IP from a given network

  Args:
      network ([type]): [description]
  """
  first = 0
  last = 0

  # If the network sent is a /32 it will only have 1 entry so first and last
  # will each remain 0 for indexing, else we drop into this conditional
  if network.num_addresses != 1:
    first = 1
    last = (network.num_addresses)-2

  # Generate a random number between the calculated first and last hosts in the subnet
  random.seed(a=None)
  ipSeed = random.randint(first,last)

  return network[ipSeed]




def sendLog(host, port, msg):
  """
  Send a syslog msg using UDP to the specified host on the specified port

  :param host: host ip/name to send the syslog message to
  :type host: string

  :param port: port number to send UDP syslog msg to
  :type port: integer

  :param msg: the message to send in the syslog
  :type msg: string

  """
  sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)   # UDP
  sock.sendto(bytes(msg, "utf-8"), (host, port))
  sock.close()


def randomLine(file):
  """
  Pull a random line from the specified file

  :param file: fully qualified path to file to pull the line from
  :type file: string

  """
  line = next(file)
  for num, aline in enumerate(file):
      if random.randrange(num + 2): continue
      line = aline
  return line


def calcDate(direction,ndays=0,dateFormat="n"):
  """
  Calculate the date as of now, days in the past or days in the future

  Args:
      direction (string): Valid options are
                          "past" - calculate timestamp to n days in the past
                          "now" - calculate the current timestamp
                          "future" - calculate the timestamp n days in the future
      ndays (int, optional): Number of days past or future to calculate the timestamp. Defaults to 0.
      dateFormat (str, optional): If "T" is sent it will return the "%Y/%m/%d %H:%M:%SZ" format.
                                  If nothing is sent (default) it will return the "%Y/%m/%d %H:%M:%SZ" format.

  Returns:
      string: the calculated date timestamp in the format specified
  """
  if dateFormat == "T":
      if direction == "past":
          return f"{datetime.now(tz=timezone.utc) - timedelta(days=ndays):%Y-%m-%dT%H:%M:%S}"
      elif direction == "now":
          return f"{datetime.now(tz=timezone.utc):%Y-%m-%dT%H:%M:%SZ}"
      else:
          return f"{datetime.now(tz=timezone.utc) + timedelta(days=ndays):%Y-%m-%dT%H:%M:%S}"
  else:
      if direction == "past":
          return f"{datetime.now(tz=timezone.utc) - timedelta(days=ndays):%Y/%m/%d %H:%M:%S}"
      elif direction == "now":
          return f"{datetime.now(tz=timezone.utc):%Y/%m/%d T%H:%M:%SZ}"
      else:
          return f"{datetime.now(tz=timezone.utc) + timedelta(days=ndays):%Y/%m/%d %H:%M:%S}"


def strTimeProp(start, end, dateFormat, prop):
  """
  Get a time at a proportion of a range of two formatted times.

  Args:
      start (string): strftime-style timestamp of the start time
      end (string): strftime-style timestamp of the end time
      dateFormat (string): specifies the returned timestamp format
      prop (string): specifies how a proportion of the interval to be taken
                      after start.
  """

  stime = time.mktime(time.strptime(start, dateFormat))
  etime = time.mktime(time.strptime(end, dateFormat))
  ptime = stime + prop * (etime - stime)
  return time.strftime(dateFormat, time.localtime(ptime))


def randomDate(start, end, prop,dateFormat='%Y-%m-%dT%H:%M:%S'):
  """
  Generate a random date string in the given format

  Args:
      start (string): Timestamp start boundary time
      end (string): Timestamp stop boundary time
      prop (string): the proportion after the start time to get timestamp
      dateFormat (str, optional): Timestamp format. Defaults to '%Y-%m-%dT%H:%M:%S'.

  Returns:
      string: the timestamp in the requested format
  """
  return strTimeProp(start, end, dateFormat, prop).strip()


def returnTime(now, t5):
  # return_time formats string for use with PSM API start and endtime

  fmonth = now.strftime("%m")
  day = now.strftime("%d")
  hour = now.strftime("%H")
  minute = now.strftime("%M")
  second = now.strftime("%S")
  tDash = f"{now.year}-{fmonth}-{day}T{hour}:{minute}:{second}Z"
  fmonth = t5.strftime("%m")
  hour = t5.strftime("%H")
  minute = t5.strftime("%M")
  second = t5.strftime("%S")

  tminusDash = f"{now.year}-{fmonth}-{day}T{hour}:{minute}:{second}Z"

  return tDash, tminusDash
