<img src="https://www.amd.com/system/files/styles/992px/private/2022-05/1423875-pensando-logo-white-1260x709_0.png?itok=D5gjoCQP" alt="AMD Pensando" width="350"/>

# PENTOOLS

---
**NOTE**

This branch is compatible with (mimics/converts) the following software.

CXOS: 10.11 <br/>
PSM:  1.54.1-T-7
NAME: captain-marvel

If these do not match your current install, [check one of the other branches](https://gitlab.com/pensando/tbd/utilities/pentools/-/branches)



# Running pentools locally:

It is recommended that you use the release branch for your current release that you want to emulate the logs from.  The latest release *should* be equivalent to what is in main, but it is guaranteed to be in the named branch.

- Clone this repository and cd into the newly created directory

    ```git clone https://gitlab.com/pensando/tbd/utilities/pentools.git```

- Install a new virtualenv  (must use python3.6 or newer)

    ```python3 -m venv .venv```

- activate the virtual environment  (your prompt should now have (.venv) in front of it)

    ```. .venv/bin/activate```

- upgrade pip

    ```pip install -U pip```

- install requirements

    ```pip install -r requirements.txt```

- the cli should work now

    ```./pentools --help```

- type deactivate to exit the venv and return to the regular terminal when you are done

## Examples
Get help on generating Firewall logs (this will display the defaults):

```./pentools fw --help```

### Basic traffic generation
Generate 1M Taormina Firewall logs between the dates of Aug 1,2021 at 1 sec past midnight and Oct 30,2021 at 11PM.
This uses the defaults of localhost and port 5514 for pensando-ELK, yours may differ:

```./pentools fw dss --date_start "2021-08-01T00:00:01" --date_end "2021-10-30T23:00:00" --tne 1000000```

Same as above but send to a different Logstash host on port 5514:

```./pentools fw dss --host 192.168.17.100 --date_start "2021-08-01T00:00:01" --date_end "2021-10-30T23:00:00" --tne 1000000```

### Anomaly traffic generation
Sometimes we want to specifically dictate what traffic/logs we are getting to provide data for a scripted demo (like the Big Red Button) or to send traffic that would be "out of the norm" and we need to specify it so that collectors, who are looking for this data, can find it.  Easiest way to do this is to build an "anomaly file" and send that instead of a config.
To do this, we will need to specify the *--anomaly* flag and the name of the anomaly in the provided config file (default is the local .penrc).  There are 2 example anomly files (telnet.allow and portscan.deny) in the anomalies directory and these anomalies, along with rdp.allow and clear_brb.allow anonmalies, are defined in the example .penrc in this repostory.  These are examples that you can base new ones upon.

Run the portscan in a very small window - will produce a spike on any graphing engine that sees the data

``` ./pentools fw dss --date_start "2022-06-22T23:50:00" --date_end "2022-06-22T23:59:00"  --anomaly portscan.deny --config anomalies/portscan.deny.json ```

Run the telnet anomaly - using the default .penrc config - with about a thousand telnets for the month of June. This is usually "hidden" in the rest of the chatter but if found, will show that a rule needs to be made to deny telnet.

``` ./pentools fw dss --date_start "2022-06-01T00:50:00" --date_end "2022-06-30T23:59:00"  --anomaly telnet.allow --tne 1000```

Reset the policy in PSM for the [Big Red Button demo](https://gitlab.com/pensando/tbd/utilities/bob).  This works by sending a log that looks like a ping to the host 1.2.3.4.  The demo system, by default, watches for this log and sees it as way to "undo" everything that it has done.

``` ./pentools fw dss --host brb_demo --port 9998 --anomaly clear_brb.allow```


## Support
If you have an issue/comment, please send email by clicking [HERE](mailto:contact-project+pensando-tbd-utilities-pentools-27550072-issue-@incoming.gitlab.com)

## Support Policy
The code and templates in the repo are released under an as-is, best effort, support policy. These scripts should be seen as community supported and Pensando will contribute our expertise as and when possible. We do not provide technical support or help in using or troubleshooting the components of the project through our normal support options. Unless explicitly tagged, all projects or work posted in our GitLab repository (at https://gitlab.com/Pensando) or sites other than our official Downloads page on https://support.pensando.io are provided under the best effort policy.
